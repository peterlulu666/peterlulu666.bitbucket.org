/*
*
* © 2015 Northeastern University
* College of Computer and Information Science (CCIS)
*
* chapter url map object
*
*/



function getURL(chapter){
    var chap = chapter;
    
    var url_map = {

            chap_1: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-4.html#node_chap_1",
            chap_2: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-5.html#node_chap_2",
            chap_3: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-6.html#node_chap_3",
            chap_4: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-7.html#node_chap_4",
            chap_5: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-8.html#node_chap_5",
            chap_6: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-9.html#node_chap_6",
            chap_7: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-10.html#node_chap_7",
            chap_8: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-11.html#node_chap_8",
            chap_9: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-13.html#node_chap_9",
            chap_10: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-14.html#node_chap_10",
            chap_11: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-15.html#node_chap_11",
            chap_12: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-16.html#node_chap_12",
            chap_13: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-17.html#node_chap_13",
            chap_14: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-19.html#node_chap_14",
            chap_15: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-20.html#node_chap_15",
            chap_16: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-21.html#node_chap_16",
            chap_17: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-22.html#node_chap_17",
            chap_18: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-23.html#node_chap_18",
            chap_19: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-25.html#node_chap_19",
            chap_20: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-26.html#node_chap_20",
            chap_21: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-27.html#node_chap_21",
            chap_22: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-28.html#node_chap_22",
            chap_23: "Incorrect URL.",
            chap_24: "Incorrect URL.",
            chap_25: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-32.html#node_chap_25",
            chap_26: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-33.html#node_chap_26",
            chap_27: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-34.html#node_chap_27",
            chap_28: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-35.html#node_chap_28",
            chap_29: "Incorrect URL.",
            chap_30: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-38.html#node_chap_30",
            chap_31: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-39.html#node_chap_31",
            chap_32: "http://htdp.org/2003-09-26/Book/curriculum-Z-H-40.html#node_chap_32",
            chap_33: "Incorrect URL.",
            chap_34: "Incorrect URL.",
            chap_35: "Incorrect URL.",
            chap_36: "Incorrect URL.",

    }
    return url_map[chap];

} // END get url